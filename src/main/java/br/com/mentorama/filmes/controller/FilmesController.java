package br.com.mentorama.filmes.controller;

import br.com.mentorama.filmes.entities.Filmes;
import br.com.mentorama.filmes.service.IFilmesService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.ArrayList;

@RestController
@RequestMapping("/filmes")
public class FilmesController {

    private final List<Filmes> filmes;

    private IFilmesService filmesService;

    public FilmesController(IFilmesService filmesService){
        this.filmesService = filmesService;
        this.filmes = new ArrayList<>();
    }

    @GetMapping
    public List<Filmes> findAll(@RequestParam(required = false) String filmes){
        return filmesService.findAll(filmes);
    }

    @GetMapping("/{id}")
    public Filmes fidById(@PathVariable("id") Integer id){
        return filmesService.findById(id);
    }

    @PostMapping
    public ResponseEntity<Integer> add(@RequestBody final Filmes filmes){
        Integer id = filmesService.add(filmes);
        return new ResponseEntity<>(id, HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity update(@RequestBody final Filmes filmes){
        filmesService.update(filmes);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable("id") Integer id){
        filmesService.delete(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

}
