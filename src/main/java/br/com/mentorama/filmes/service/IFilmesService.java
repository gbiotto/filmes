package br.com.mentorama.filmes.service;

import br.com.mentorama.filmes.entities.Filmes;

import java.util.List;

public interface IFilmesService {

    List<Filmes> findAll(String filmes);
    Filmes findById(Integer id);
    void update(final Filmes filmes);
    void delete(Integer id);
    Integer add(final Filmes filmes);
}
