package br.com.mentorama.filmes.service;

import br.com.mentorama.filmes.entities.Filmes;
import br.com.mentorama.filmes.processor.Processor;
import br.com.mentorama.filmes.repository.FilmesRepository;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class FilmesService implements IFilmesService{
    
    private FilmesRepository filmesRepository = FilmesRepository.getInstance();

    private List<Processor> processos;

    public List<Filmes> findAll(String nomeFilme){
        if(nomeFilme != null){
            return filmesRepository.findAll(nomeFilme);
        }
        return filmesRepository.findAll();
    }

    public Filmes findById(Integer id){
        return  filmesRepository.findById(id);
    }
    
    public Integer add(final Filmes filmes){
        if(filmes.getId() == null){
            filmes.setId(filmesRepository.count() + 1);
        }
        processos.stream().forEach(processo -> processo.process(filmes));
        filmesRepository.add(filmes);
        return filmes.getId();
    }

    public void update(final Filmes filmes){
        filmesRepository.update(filmes);
    }

    public void delete(Integer id){
        filmesRepository.delete(id);
    }
    
}
