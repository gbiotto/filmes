package br.com.mentorama.filmes.entities;

public class Filmes {

    private  Integer id;
    private String nomeFilme;
    private String nomeDiretor;
    private Integer ano;
    private Integer nota;

    public Filmes(Integer id, String nomeFilme, String nomeDiretor) {
        this.id = id;
        this.nomeFilme = nomeFilme;
        this.nomeDiretor = nomeDiretor;
    }

    public Filmes(Integer id, String nomeFilme, String nomeDiretor, Integer ano, Integer nota) {
        this.id = id;
        this.nomeFilme = nomeFilme;
        this.nomeDiretor = nomeDiretor;
        this.ano = ano;
        this.nota = nota;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNomeFilme() {
        return nomeFilme;
    }

    public void setNomeFilme(String nomeFilme) {
        this.nomeFilme = nomeFilme;
    }

    public String getNomeDiretor() {
        return nomeDiretor;
    }

    public void setNomeDiretor(String nomeDiretor) {
        this.nomeDiretor = nomeDiretor;
    }

    public Integer getAno() {
        return ano;
    }

    public void setAno(Integer ano) {
        this.ano = ano;
    }

    public Integer getNota() {
        return nota;
    }

    public void setNota(Integer nota) {
        this.nota = nota;
    }

    @Override
    public String toString() {
        return "Message{" +
                "id=" + id +
                ", nomeFilme='" + nomeFilme + '\'' +
                ", nomeDiretor='" + nomeDiretor + '\'' +
                ", ano=" + ano +
                ", nota=" + nota +
                '}';
    }
}
