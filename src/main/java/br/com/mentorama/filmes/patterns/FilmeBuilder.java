package br.com.mentorama.filmes.patterns;
import br.com.mentorama.filmes.entities.Filmes;

public class FilmeBuilder {

    private  Integer id;
    private String nomeFilme;
    private String nomeDiretor;
    private Integer ano;
    private Integer nota;

    public FilmeBuilder withId(Integer id) {
        this.id = id;
        return this;
    }

    public FilmeBuilder withNomeFilme(String nomeFilme) {
        this.nomeFilme = nomeFilme;
        return this;
    }

    public FilmeBuilder withNomeDiretor(String nomeDiretor) {
        this.nomeDiretor = nomeDiretor;
        return this;
    }

    public FilmeBuilder withAno(Integer ano) {
        this.ano = ano;
        return this;
    }

    public FilmeBuilder withNota(Integer nota) {
        this.nota = nota;
        return this;
    }

    public Filmes build() {
        return new Filmes(id, nomeFilme, nomeDiretor, ano, nota);
    }
}
