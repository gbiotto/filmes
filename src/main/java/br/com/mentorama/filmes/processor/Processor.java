package br.com.mentorama.filmes.processor;

import br.com.mentorama.filmes.entities.Filmes;

public interface Processor {

    void process(Filmes filmes);
}
