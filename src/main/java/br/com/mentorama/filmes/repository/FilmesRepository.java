package br.com.mentorama.filmes.repository;

import br.com.mentorama.filmes.entities.Filmes;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class FilmesRepository {

    private static FilmesRepository filmesRepository;

    private FilmesRepository(){
        this.filmes = new LinkedList<>();
    }

    public static  FilmesRepository getInstance(){
        if(filmesRepository == null){
            filmesRepository = new FilmesRepository();
        }
        return filmesRepository;
    }

    private final List<Filmes> filmes;

    public List<Filmes> findAll(){
        return filmes;
    }
    public List<Filmes> findAll(final String nomeFilme){
        return filmes.stream()
                .filter(flm -> flm.getNomeFilme().contains(nomeFilme))
                .collect(Collectors.toList());
    }

    public Filmes findById(Integer id){
        return this.filmes.stream()
                .filter(flm -> flm.getId().equals(id))
                .findFirst()
                .orElse(null);
    }

    public void update(final Filmes filmes){
        this.filmes.stream()
                .filter(flm -> flm.getId().equals(filmes.getId()))
                .forEach(flm -> flm.setNomeFilme(filmes.getNomeFilme()));
    }

    public void delete(Integer id){
        filmes.removeIf(flm -> flm.getId().equals(id));
    }

    public void add(Filmes filmes){
        this.filmes.add(filmes);
    }

    public int count(){
        return filmes.size();
    }
}
